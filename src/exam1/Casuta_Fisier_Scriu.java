package exam1;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Casuta_Fisier_Scriu extends JFrame {

        JFrame frame;

        Casuta_Fisier_Scriu(String Title){
            super(Title);
            frame=this;
            setSize(250,250);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setVisible(true);
        }

        void setup()
        {
            this.init();
            this.setLayout(null);

            JTextField jtf1=new JTextField();
            JButton jb=new JButton("write");
            JTextField jtf2= new JTextField();

            jtf1.setBounds(10,10,200,30);
            jtf2.setBounds(10,50,200,30);
            jb.setBounds(10,100,100,30);

            jb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    try{
                        String path = "C:\\Users\\Denicel\\examen_se_denisa\\src\\exam1\\";
                        FileWriter fw=new FileWriter(path+jtf1.getText());
                        BufferedWriter writer=new BufferedWriter(fw);

                        writer.write(jtf2.getText());
                        writer.flush();
                        writer.close();
                    }
                    catch (IOException ioe){
                        System.err.println(ioe);
                        System.exit(1);

                    }
                }
            });

            add(jb);
            add(jtf1);
            add(jtf2);
        }

        void init()
        {

        }

        public static void main(String[] args) {
            Casuta_Fisier_Scriu ui = new Casuta_Fisier_Scriu("a");
            ui.setup();
        }

}
