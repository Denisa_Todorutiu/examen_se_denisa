package exam1;

import java.util.ArrayList;

public class Bank {

        private String name;
        ArrayList<BankAccount> members;

        public Bank(String name) {
            this.name = name;
            members = new ArrayList<>();

}

     class BankAccount {

        private String owner;
        private double balance;

        public BankAccount(String owner, double balance) {
            this.owner = owner;
            this.balance = balance;
        }

        public String getOwner() {
            return owner;
        }

        public double getBalance() {
            return balance;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }



          static class Main{
             public static void main(String[] args) {


                 Bank bcr = new Bank("Banca Comerciala Romana");
             }}